package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
)

var uptime = time.Now()

func info(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(map[string]interface{}{
		"ok":     true,
		"uptime": time.Since(uptime),
		"name":   "user",
	})
}

func getenv(env, defaultVal string) string {
	if val := os.Getenv(env); val != "" {
		return val
	}

	return defaultVal
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", info)

	srv := &http.Server{
		Handler:      r,
		Addr:         fmt.Sprintf(":%s", getenv("PORT", "8000")),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}
