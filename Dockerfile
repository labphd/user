FROM golang

WORKDIR /go/src/app
COPY . .

ENV PORT 80
EXPOSE 80

RUN go get ./...
RUN go build -o app && chmod +x ./app

CMD ["./app"]